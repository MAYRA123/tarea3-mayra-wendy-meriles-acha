/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica3;

/**
 *
 * @author pc
 */
public class COLA<T> {
    private NODO<T> first;
    private NODO<T> last;
    private int size;
    
    public COLA(){
    this.first=null;
     this.last=null;
     this.size = 0;
    }
    public boolean isEmpty(){
    return first == null;
    }
    public int sizeQueue(){
    return size;
    
    }
    public T firstElement(){
    if(!isEmpty()){
    return null;
     }
    return first.getElement();
    }
    public void enqueue(T element){
    NODO<T> newelement = new NODO(element,null);
    if(isEmpty()){//[1]->[2] -> [3] ->[4]
    first=newelement;
    last=newelement;
     }else{
       if(sizeQueue() == 1 ){
       first.setNext(newelement);
       }else{
       last.setNext(newelement);
       }
       last = newelement;
     }
    }
    public T dequeue(){
    if(isEmpty()){
    return null;
    }else{//[2]->[3]
        T element = first.getElement();//1
        NODO<T> aux = first.getNext();//[2]
        first = aux ;
        size--;
        if(isEmpty()){
        last = null;
        }
    return element;
    }
  }
}
