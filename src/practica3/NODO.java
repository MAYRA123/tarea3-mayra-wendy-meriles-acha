package practica3;
public class NODO<T> {
    private T elem;
    private NODO<T> next;
    public NODO(T elem, NODO<T> next){
        this.elem = elem;
        this.next = next;
    }

    public T getElement() {
        return elem;
    }

    public void setElement(T elem) {
        this.elem = elem;
    }

    public NODO<T> getNext() {
        return next;
    }

    public void setNext(NODO<T> next) {
        this.next = next;
    }   
    
}